<!--🖇🖇🖇🖇🖇-->
 <p align="center">
   <img width="100" height="100" src="./assets/linux.png" alt="Logo">
  </p>
  <h1 align="center"><b>Master Linux</b></h1>
  <h3 align="center"><b>The most comprehensive guide to deal with all Linux things and commands.</b></h3>
  <h3 align="center"><b>This repo is under development, you can try to contribute.</b></h3>
<!--🖇🖇🖇🖇🖇-->
<br />
<p align="center">
  Here is you can get started with Linux.
  This tutorial provides basic and advanced concepts of Linux, you can learn for free.
  You can also help us by contributing this repository, and also try to solve any mistakes written in documentation. <br/>
  LINUX is an operating system or a kernel distributed under an open-source license. In simple words, an operating system is a software that enables the communication between computer hardware and software.
    <br />
    <br />
    <a href="./Why/whylinux.md"><strong>Why Linux?</strong></a><br />
    <a href="./History/history.md"><strong>History</strong></a><br />
    <a href="./LinuxEverywhere/linuxeverywhere.md"><strong>Linux Everywhere</strong></a><br />
    <a href="./Distros/distros.md"><strong>Linux Distro</strong></a><br />
    <a href="./DesktopEnv/desktopenv.md"><strong>Desktop Environment</strong></a><br />
    <a href="./commadline/commandline.md"><strong>Command Line</strong></a><br />
    <a href="#"><strong>Users and root</strong></a><br />
    <a href="#"><strong>Package Management</strong></a><br />
    <a href="#"><strong>Processes</strong></a><br />
    <a href="#"><strong>File Management</strong></a><br />
    <a href="#"><strong>Devices</strong></a><br />
    <a href="#"><strong>System Partition</strong></a><br />
    <a href="#"><strong>Bootloader and startup</strong></a><br />
    <a href="#"><strong>Linux Kernel</strong></a><br />
    <a href="#"><strong>Drivers</strong></a><br />
    <a href="#"><strong>var and logs</strong></a><br />
    <a href="#"><strong>Networkng basics</strong></a><br />
    <a href="#"><strong>Network sharing and config</strong></a><br />
    <a href="#"><strong>Routing</strong></a><br />
    <a href="#"><strong>DNS</strong></a><br />
    <a href="#"><strong>Configuratons</strong></a><br />
    <br /> <br />
